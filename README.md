# Shaker Algo #

### UiPath Studio ###

* Scheme

![Scheme](img/1.PNG)

Here, you can see, simple scheme


* Enter data

![Enter_data](img/2.PNG)

You must enter only integers or 'End' for stoped adding data


* Error

![Error](img/3.PNG)

If you try to input not integer of 'End', you will see exception.

* Data was added

![OK_Add](img/4.PNG)

After 'End' you will see, what your data will added


* Data in List

![Scheme](img/5.PNG)

* Data in List after Sorting

![Scheme](img/6.PNG)